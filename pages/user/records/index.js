import { Table, Button } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper';
import { useState, useEffect} from'react';

const recordIndex = (props) => {
    const [data, setData] = useState(null)
    useEffect(async()=>{
        if(AppHelper.getAccessToken()==undefined){
            return;
        }
        const payload = {
            method : 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            }
        }
        console.log(AppHelper.API_URL)
        const res = await fetch(`https://shielded-river-77371.herokuapp.com/api/users/get-records-breakdown-by-range`,payload)
        const records = await res.json()
        console.log(records)
        setData( records )

        
    },[])

    return (
        <View title="Records">
            <h3>Records</h3>
            <Link href="/user/records/new">
                <a className="btn btn-success mt-1 mb-3">Add</a>
            </Link>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    {data?data.map(record=>(<RecordsView data={record}/>)):<></>}
                </tbody>
            </Table>
        </View>
    )
}

export default recordIndex;

const RecordsView=({data})=>{ 
    console.log(data)
    
    return (
        <>
            <tr>
                <td>{data.categoryName}</td>
                <td>PHP {data.totalAmount}</td>
            </tr>
        </>
    )
}