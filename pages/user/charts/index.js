import { Card, Button, Row, Col, InputGroup, FormControl, Form, Alert } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper'
import { useState, useEffect } from 'react'
import { Pie } from 'react-chartjs-2'
import moment from 'moment'

export default () => {
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))
    const [categoryType, setCategoryType] = useState([])

     const data = {
        labels: labelsArr,
        datasets: [ 
            {
                data: dataArr,
                backgroundColor: [
                '#FF9AA2',
                '#FFB7B2',
                '#FFDAC1',
                '#E2F0CB',
                '#B5EAD7',
                '#C7CEEA',
                '#CC99FF'                 
                ],
                hoverBackgroundColor: [
                '#FF9AA2',
                '#FFB7B2',
                '#FFDAC1',
                '#E2F0CB',
                '#B5EAD7',
                '#C7CEEA',
                '#CC99FF' 
                ]
            }
        ]
    };

 useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }

        fetch(`https://shielded-river-77371.herokuapp.com/api/users/get-records-breakdown-by-range`, payload).then(AppHelper.toJSON).then(records => {
  // ******* This will change the values of the component base from tge information in the database ******** // 
            setLabelsArr(records.map(record => record.categoryName)) 
            setDataArr(records.map(record => record.totalAmount))
        })
    }, [fromDate, toDate])

    return (
        <View title="Records">
            <h3>Records</h3>
            <InputGroup className="mb-2">
                <InputGroup.Prepend>
                    <Link href="/user/records/new">
                        <a className="btn btn-success">Add</a>
                    </Link>
                </InputGroup.Prepend>
                <Form.Control type="date" value={ fromDate } onChange={ (e) => setFromDate(e.target.value) }/>
                <Form.Control type="date" value={ toDate } onChange={ (e) => setToDate(e.target.value) }/>
                <Pie data={ data }/>
            </InputGroup>
            <RecordsView />
        </View>
    )
}

const RecordsView = () => {   
    return (
        <>
              
                        <Card className="mb-3">
                            <Card.Body>
                                <Row>
                                    <Col xs={ 12 } className="text-center">
                                        <Alert variant="info">My Budget Breakdown</Alert>
                                        <span></span>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    
        </>
    )
}